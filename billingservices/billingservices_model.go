package billingservices

import "github.com/Kamva/mgm"

type configurationForm struct {
	IsPartialPaymentAllowed bool   `json:"isPartialPaymentAllowed" bson:"isPartialPaymentAllowed"`
	ExternalID              string `json:"externalId" bson:"externalId"`
	IsBatchProcessing       bool   `json:"isBatchProcessing" bson:"isBatchProcessing"`
	IsAnnullable            bool   `json:"isAnulable" bson:"isAnulable"`
	QueryFormType           string `json:"queryFormType" bson:"queryFormType"`
	MinimunPaymentAmount    int    `json:"minimunPaymentAmount" bson:"minimunPaymentAmount"`
	Provider                string `json:"provider" bson:"provider"`
	Priority                int    `json:"priority" bson:"priority"`
	// QueryForm               string `json:"queryForm" bson:"queryForm"`
}

// Billingservices structures required model for db
type Billingservices struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string              `json:"name" bson:"name"`
	CategoryID       string              `json:"categoryId" bson:"categoryId"`
	CompanyID        string              `json:"companyId" bson:"companyId"`
	Configuration    []configurationForm `json:"configuration" bson:"configuration"`
	IsActive         bool                `json:"isActive" bson:"isActive"`
}

// NewBillingServices builds a new billingservices model
func NewBillingServices(
	Name string,
	CategoryID string,
	CompanyID string,
	Configuration []configurationForm,
	IsActive bool,
) *Billingservices {
	return &Billingservices{
		Name:          Name,
		CategoryID:    CategoryID,
		CompanyID:     CompanyID,
		Configuration: Configuration,
		IsActive:      IsActive,
	}
}
