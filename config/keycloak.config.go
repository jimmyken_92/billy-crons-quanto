package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/Nerzal/gocloak"
)

// func loads keycloak credentials
func loadJSON() Keycloak {
	pwd, _ := os.Getwd()
	jsonFile, err := os.Open(pwd + os.Getenv("KEYCLOAK_CONFIG_FILE"))
	if err != nil {
		log.Fatal("Error loading keycloak configuration.\nPlease check if file with keycloak configurations exists")
	}

	// `defer` statement defers the execution of a function until the surrounding function returns
	// The deferred call's arguments are evaluated immediately
	// but the function call is not executed until the surrounding function returns
	defer jsonFile.Close()

	bytes, _ := ioutil.ReadAll(jsonFile)

	var keycloak Keycloak
	json.Unmarshal(bytes, &keycloak)

	// remove unnecessary chars in url
	keycloak.AuthServerURL = strings.TrimSuffix(keycloak.AuthServerURL, "/auth/")

	return keycloak
}

// KeycloakSignin starts a new keycloak session
func KeycloakSignin() string {
	config := loadJSON()
	var clientID, clientSecret, realm = config.Resource, config.Credentials.Secret, config.Realm
	client := gocloak.NewClient(config.AuthServerURL)
	token, err := client.LoginClient(clientID, clientSecret, realm)
	if err != nil {
		log.Fatal("Failed to log into Keycloak. Please check your configuration data, or try again later.")
	}

	fmt.Println("Keycloak: Login successful")
	var _, mapTokenInfo, _ = client.DecodeAccessToken(token.AccessToken, realm)
	var verifyExpiresTime = mapTokenInfo.VerifyExpiresAt(int64(token.ExpiresIn), false)
	fmt.Println(verifyExpiresTime)

	return token.AccessToken
}
