package config

// Keycloak define configuration content
type Keycloak struct {
	Realm                    string      `json:"realm"`
	AuthServerURL            string      `json:"auth-server-url"`
	SSLRequired              string      `json:"ssl-required"`
	Resource                 string      `json:"resource"`
	VerifyTokenAudience      string      `json:"verify-token-audience"`
	UserResourceRolemappings bool        `json:"use-resource-role-mappings"`
	Credentials              Credentials `json:"credentials"`
	ConfidentialPort         int         `json:"confidential-port"`
}

// Credentials define keycloak credentials config
type Credentials struct {
	Secret string `json:"secret"`
}
