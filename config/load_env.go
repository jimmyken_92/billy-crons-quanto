package config

import (
	"log"

	"github.com/joho/godotenv"
)

// Load application environments
func LoadEnvironments() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}
