package database

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// ConnectDatabase setup database context in a specified url and database name
func ConnectDatabase() (context.Context, *mongo.Client) {
	// get configuration from system environments
	uri := os.Getenv("DATABASE_URL")

	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	if cancel == nil {
		log.Fatal("[ERR] Database context error: ", cancel)
	}

	conn := client.Connect(ctx)
	if conn != nil {
		log.Fatal("[ERR] Database client connection error: ", conn)
	}

	fmt.Println("[INFO] Database connected...")
	return ctx, client
}
