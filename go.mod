module quanto.la/billy/crons

go 1.15

require (
	github.com/Kamva/mgm v1.2.3
	github.com/Nerzal/gocloak v1.0.0 
	github.com/bradleyjkemp/cupaloy/v2 v2.6.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/joho/godotenv v1.3.0
	go.mongodb.org/mongo-driver v1.3.4
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
