package main

import (
	"quanto.la/billy/crons/config"
	"quanto.la/billy/crons/database"
	"quanto.la/billy/crons/pendingpayments"
)

func main() {
	config.LoadEnvironments()
	var token = config.KeycloakSignin()
	pendingpayments.AccessToken = token
	ctx, client := database.ConnectDatabase()
	pendingpayments.Find(ctx, client)
}
