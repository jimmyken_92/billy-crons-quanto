package payments

type BillingServicesProvider string

const (
	PractipagoProvider BillingServicesProvider = "PRACTIPAGO"
)

type PaymentStatus string

const (
	PaymentStatusPending    PaymentStatus = "PENDING"
	PaymentStatusConfirmed  PaymentStatus = "CONFIRMED"
	PaymentStatusAuthorized PaymentStatus = "AUTHORIZED"
	PaymentStatusFailed     PaymentStatus = "FAILED"
	PaymentStatusRejected   PaymentStatus = "REJECTED"
	PaymentStatusReversed   PaymentStatus = "REVERSED"
)
