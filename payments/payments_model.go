package payments

import (
	"time"

	"github.com/Kamva/mgm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type FormSchema struct {
	Form            string               `json:"form" bson:"form"`
	RawResponse     interface{}          `json:"rawResponse" bson:"rawResponse"`
	BillInformation []primitive.ObjectID `json:"billInformation" bson:"billInformation"`
	SelectedBillId  string               `json:"selectedBillId" bson:"selectedBillId"`
	UserData        interface{}          `json:"userData" bson:"userData"`
}

type Amount struct {
	Value    int    `json:"value" bson:"value"`
	Currency string `json:"currency" bson:"currency"`
}

type Payments struct {
	mgm.DefaultModel `bson:",inline"`
	Status           PaymentStatus           `json:"status" bson:"status"`
	Origin           string                  `bson:"origin" json:"origin"`
	UserSub          string                  `json:"userSub" bson:"userSub"`
	BillService      primitive.ObjectID      `json:"billService" bson:"billService"`
	Provider         BillingServicesProvider `json:"provider" bson:"provider"`
	Amount           Amount                  `json:"amount,omitempty" bson:"amount"`
	CreatedAt        time.Time               `bson:"createdAt" json:"createdAt"`
	UpdatedAt        time.Time               `bson:"updatedAt" json:"updatedAt"`
	// Query                   FormSchema	 `json:"query" bson:"query"`
	// Confirmation            FormSchema `json:"confirmation" bson:"confirmation"`
	// TransactionRef          string     `json:"transactionRef" bson:"transactionRef"`
	// OperationNumber         string     `json:"operationNumber" bson:"operationNumber"`
	// ExternalOperationNumber string     `json:"externalOperationNumber" bson:"externalOperationNumber"`
	// ErrorReason             string     `json:"errorReason" bson:"errorReason"`
	// ExternalResponse        string     `json:"externalResponse" bson:"externalResponse"`
}

func (payment *Payments) Save() error {
	return mgm.Coll(payment).Create(payment)
}

func NewPaymentDocument(
	billService primitive.ObjectID,
	provider BillingServicesProvider,
	userSub string,
) *Payments {
	return &Payments{
		BillService: billService,
		Provider:    provider,
		UserSub:     userSub,
		Origin:      "SYSTEM",
		Status:      PaymentStatusPending,
	}
}

// var PaymentsCollection = mgm.Coll(&Payments{})
