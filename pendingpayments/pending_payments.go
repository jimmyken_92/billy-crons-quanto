package pendingpayments

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	b "gopkg.in/mgo.v2/bson"

	"quanto.la/billy/crons/payments"
	"quanto.la/billy/crons/practipago_wsdl"
)

const (
	// successfully confirmed
	confirmationMessageSuccess string = "payment-confirmed"
	// reversed
	confirmationMessageFailed string = "payment-failed"
	// reversed by timeout
	confirmationMessageTimeout string = "payment-timeout"
)

// AccessToken from keycloak
var AccessToken string

// HTTPClient provides http client for endpoints
var HTTPClient = &http.Client{}

type respbody struct {
	Status string `json:"status"`
}

type users struct {
	Message       string
	Data          nationalDocument
	Status        string
	PhoneVerified bool
	Nickname      string
	PhoneNumber   string
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeviceID      string
}

type nationalDocument struct {
	Document document
}

type document struct {
	Type       string
	Identifier int
}

// Find retrieves data from db
func Find(ctx context.Context, client *mongo.Client) {
	databaseName := os.Getenv("DATABASE_NAME")

	database := client.Database(databaseName)
	collection := database.Collection(os.Getenv("DATABASE_COLLECTION"))

	match := bson.D{{"$match", bson.M{"status": "CONFIRMATION_PENDING"}}}
	lookup := bson.D{{"$lookup", bson.D{{"from", "billingservices"}, {"localField", "billService"}, {"foreignField", "_id"}, {"as", "services"}}}}

	info, e := collection.Aggregate(ctx, mongo.Pipeline{match, lookup})
	if e != nil {
		panic(e)
	}
	var res []bson.M
	if err := info.All(ctx, &res); err != nil {
		log.Fatal(err)
	}
	paymentsHandler(res)
}

func paymentsHandler(paymentsResult []bson.M) {
	fmt.Println("[INFO] Successfully retrieved data")

	// payments
	for _, payment := range paymentsResult {
		p := &payments.Payments{}
		paymentBsonToBytes, _ := b.Marshal(payment)
		b.Unmarshal(paymentBsonToBytes, p)
		practipagoPaymentConfirmation(p)
	}
}

// confirm payment in Practipago
func practipagoPaymentConfirmation(payment *payments.Payments) {
	id := payment.GetID().(primitive.ObjectID).Hex()

	// check by time
	paymentUpdatedAt := payment.UpdatedAt
	current := time.Now()
	diff := current.Sub(paymentUpdatedAt)
	if diff > 1 {
		reverse := reverseByID(id)
		if reverse != false {
			sendNotification(confirmationMessageTimeout, payment.UserSub)
		}
	}

	// get required data
	username := os.Getenv("PRACTIPAGO_USER")
	password := os.Getenv("PRACTIPAGO_PASS")
	url := os.Getenv("PRACTIPAGO_URL")
	client := practipago_wsdl.NewPractipagoClient(practipago_wsdl.WithAuthOptions(username, password), practipago_wsdl.WithBaseURLOptions(url))

	confirmation, _ := client.CheckTransactionStatus(id)
	code, status := confirmation.Body.Response.Return.Status.Code, confirmation.Body.Response.Return.Status.Description

	if code != 0 {
		confirmation := confirmPayment(id, status)
		if confirmation != false {
			sendNotification(confirmationMessageSuccess, payment.UserSub)
		}
	} else {
		reverse := reverseByID(id)
		if reverse != false {
			sendNotification(confirmationMessageFailed, payment.UserSub)
		}
	}
}

// reverse payment
func reverseByID(id string) bool {
	var uri = os.Getenv("BANKING_URI") + "/transactions/" + id

	bearer := "Bearer " + AccessToken
	req, err := http.NewRequest("DELETE", uri, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-type", "application/json")

	resp, err := HTTPClient.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	r, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(r))
	return true
}

// confirm payment
func confirmPayment(id string, status string) bool {
	var uri = os.Getenv("BANKING_URI") + "/transactions/" + id

	var respBody respbody
	respBody.Status = status
	body, _ := json.Marshal(respBody)

	bearer := "Bearer " + AccessToken
	req, err := http.NewRequest("PATCH", uri, bytes.NewBuffer(body))
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-type", "application/json")

	resp, err := HTTPClient.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	r, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(r))
	return true
}

// send notificiation
func sendNotification(event string, userSub string) {
	var uri = os.Getenv("MESSAGE_URI")
	phoneNumber, deviceID := getUserData(userSub)

	message, _ := json.Marshal(map[string]string{
		"eventName":   event,
		"phoneNumber": phoneNumber,
		"deviceId":    deviceID,
		"userSub":     userSub,
	})

	bearer := "Bearer " + AccessToken
	req, err := http.NewRequest("POST", uri, bytes.NewBuffer(message))
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-type", "application/json")

	resp, err := HTTPClient.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
}

// get user data
func getUserData(userSub string) (string, string) {
	uri := os.Getenv("USERS_URI") + "/" + userSub
	bearer := "Bearer " + AccessToken
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-type", "application/json")

	body, _ := ioutil.ReadAll(req.Body)
	userData := &users{}
	json.Unmarshal(body, userData)

	return userData.PhoneNumber, userData.DeviceID
}
