package practipago_wsdl

import (
	"bytes"
	"crypto/tls"
	"net/http"

	"quanto.la/billy/crons/practipago_wsdl/parser"
)

type PractipagoClientOptions func(client *PractipagoClient)

type PractipagoClient struct {
	username   string
	password   string
	baseURL    string
	httpClient *http.Client
}

// NewPractipagoClient variadic constructor for create practipago client
func NewPractipagoClient(options ...PractipagoClientOptions) *PractipagoClient {
	client := &PractipagoClient{}

	for _, option := range options {
		option(client)
	}

	if client.httpClient == nil {
		client.httpClient = &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
			},
		}
	}

	return client
}

// WithAuthOptions add authentication properties to client struct
func WithAuthOptions(username string, password string) PractipagoClientOptions {
	return func(client *PractipagoClient) {
		client.username = username
		client.password = password
	}
}

func WithBaseURLOptions(baseURL string) PractipagoClientOptions {
	return func(client *PractipagoClient) {
		client.baseURL = baseURL
	}
}

func (c *PractipagoClient) GetInvoiceData(serviceId string, serviceReference string) (*parser.InvoiceData, error) {
	payload := getInvoiceDataRequest(c.username, c.password, serviceId, serviceReference)
	req, err := http.NewRequest("POST", c.baseURL, bytes.NewReader(payload))
	c.setSoapHeaders(getInvoiceDataAction, req)

	if err != nil {
		return nil, err
	}

	res, resErr := c.httpClient.Do(req)
	if resErr != nil {
		return nil, err
	}

	return parser.NewInvoiceDataFromXML(res.Body)
}

func (c *PractipagoClient) CheckTransactionStatus(trxID string) (*parser.TrxStatus, error) {
	payload := checkTransactionStatusRequest(c.username, c.password, trxID)
	req, err := http.NewRequest("POST", c.baseURL, bytes.NewReader(payload))
	c.setSoapHeaders(checkTransactionStatusAction, req)

	if err != nil {
		return nil, err
	}

	res, resErr := c.httpClient.Do(req)
	if resErr != nil {
		return nil, err
	}

	return parser.NewTrxStatusFromResponse(res.Body)
}

func (c *PractipagoClient) setSoapHeaders(action string, req *http.Request) {
	req.Header.Set("SOAPAction", action)
	req.Header.Set("Content-type", "text/xml")
}
