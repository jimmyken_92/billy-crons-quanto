package practipago_wsdl

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/bradleyjkemp/cupaloy/v2"
)

const (
	url            = "http://localhost:3000/?wsdl"
	globalUsername = "test"
	globalPassword = "test"
)

func TestNewPractipagoClient(t *testing.T) {
	client := NewPractipagoClient()

	if client == nil {
		t.Error("Expected defined structure 'PractipagoClient'")
	}
}

func TestNewPractipagoClientWithOptions(t *testing.T) {
	client := NewPractipagoClient(WithAuthOptions(globalUsername, globalPassword), WithBaseURLOptions(url))

	if client == nil {
		t.Error("Expected defined structure 'PractipagoClient'")
	}

	if client.username != globalUsername {
		t.Errorf("Expected %s, received %s", globalUsername, client.username)
	}

	if client.password != globalPassword {
		t.Errorf("Expected %s, received %s", globalPassword, client.password)
	}

	if client.baseURL != url {
		t.Errorf("Expected %s, received %s", url, client.baseURL)
	}

	if client.httpClient == nil {
		t.Error("Expected defined structure 'PractipagoClient.httpClient'")
	}
}

func TestSetSoapHeaders(t *testing.T) {
	soapAction := "TestAction"
	client := NewPractipagoClient(WithAuthOptions(globalUsername, globalPassword), WithBaseURLOptions(url))
	req, _ := http.NewRequest("POST", url, bytes.NewReader([]byte("Test only")))
	client.setSoapHeaders(soapAction, req)

	actionHeader := req.Header.Get("SOAPAction")
	contentTypeHeader := req.Header.Get("Content-type")

	if actionHeader != soapAction {
		t.Errorf("Expected %s, received %s", soapAction, actionHeader)
	}

	if contentTypeHeader != "text/xml" {
		t.Errorf("Expected %s, received %s", "text/xml", actionHeader)
	}
}

/// TODO: add structure for response
func TestGetInvoiceDataMethod(t *testing.T) {
	url := "http://138.186.63.146:9004/WSDocumenta/Documenta"
	username := "documenta"
	password := "documenta123"
	client := NewPractipagoClient(WithAuthOptions(username, password), WithBaseURLOptions(url))

	res, err := client.GetInvoiceData("3", "1111111")

	if err != nil {
		panic(err)
	}

	snapshotter := cupaloy.New(cupaloy.SnapshotFileExtension(".snapshot"), cupaloy.SnapshotSubdirectory("__snapshots__"))
	snapshotter.Snapshot(res)
}

func TestCheckTransactionStatusMethod(t *testing.T) {
	url := "http://138.186.63.146:9004/WSDocumenta/Documenta"
	username := "documenta"
	password := "documenta123"
	client := NewPractipagoClient(WithAuthOptions(username, password), WithBaseURLOptions(url))

	res, err := client.CheckTransactionStatus("123456")

	if err != nil {
		panic(err)
	}

	if res.Body.Response.Return.Status.Code != 1 {
		t.Error("Expected 1, received 0")
	}

	if res.Body.Response.Return.Status.Description != "Transaccion aprobada" {
		t.Errorf("Expected 'Transaccion aprobada', received %s", res.Body.Response.Return.Status.Description)
	}
}
