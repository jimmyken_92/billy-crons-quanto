package parser

type Flateable interface {
	Flat() (code int, message string)
}

type WSDLError struct {
	StatusCode int
	Message    string
}

func (e *WSDLError) Error() string {
	return e.Message
}

func NewErrorFromWSDLResponse(response Flateable) *WSDLError {
	code, message := response.Flat()

	if code == 1 {
		if message == "" {
			return &WSDLError{
				StatusCode: 500,
				Message:    "Internal server error",
			}
		}
		return &WSDLError{
			StatusCode: 400,
			Message:    message,
		}
	}

	return nil
}
