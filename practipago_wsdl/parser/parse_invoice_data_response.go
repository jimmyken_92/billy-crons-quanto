package parser

import (
	"encoding/xml"
	"io"
)

type Entry struct {
	Key   string `xml:"key"`
	Value struct {
		Legend  string `xml:"legend"`
		Value   string `xml:"value"`
		Visible bool   `xml:"visible"`
	} `xml:"value"`
}

type Details struct {
	Total           int    `xml:"monto"`
	Description     string `xml:"descipcion"`
	BillInformation struct {
		Container struct {
			Entries []Entry `xml:"entry"`
		} `xml:"map"`
	} `xml:"hashMapContainer"`
}

type InvoiceData struct {
	Body struct {
		Response struct {
			Return struct {
				Code             int    `xml:"codRetorno"`
				IsAmountWritable bool   `xml:"cambioMontoPermitido"`
				Message          string `xml:"mensajeOperacion"`
				Reference        string `xml:"referenciaConsulta"`
				Data             struct {
					ServiceID string    `xml:"idServicio"`
					Details   []Details `xml:"detalleReferencias"`
				} `xml:"detalleServicios"`
			} `xml:"return"`
		} `xml:"consultaResponse"`
	} `xml:"Body"`
}

func NewInvoiceDataFromXML(response io.ReadCloser) (*InvoiceData, error) {
	data := &InvoiceData{}
	decoder := xml.NewDecoder(response)
	decodeErr := decoder.Decode(data)
	if decodeErr != nil {
		return nil, decodeErr
	}

	wsdlErr := NewErrorFromWSDLResponse(data)
	if wsdlErr != nil {
		return nil, wsdlErr

	}

	return data, nil
}

func (d *InvoiceData) Flat() (code int, message string) {
	return d.Body.Response.Return.Code, d.Body.Response.Return.Message
}
