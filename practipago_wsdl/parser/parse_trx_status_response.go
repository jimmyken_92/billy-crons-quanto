package parser

import (
	"encoding/xml"
	"io"
)

type TrxStatus struct {
	Body struct {
		Response struct {
			Return struct {
				Status struct {
					Code        int    `xml:"codigoRetorno"`
					Description string `xml:"descripcion"`
					LogID       int    `xml:"idLogTransaccion"`
				} `xml:"estado"`
			} `xml:"return"`
		} `xml:"estadoTransaccionResponse"`
	} `xml:"Body"`
}

func NewTrxStatusFromResponse(response io.ReadCloser) (*TrxStatus, error) {
	data := &TrxStatus{}
	decoder := xml.NewDecoder(response)
	decodeErr := decoder.Decode(data)
	if decodeErr != nil {
		return nil, decodeErr
	}

	return data, nil
}
