package practipago_wsdl

import (
	"fmt"
	"strings"
)

func getInvoiceDataRequest(username string, password string, serviceId string, serviceReference string) []byte {
	return []byte(strings.TrimSpace(
		fmt.Sprintf(
			`
				<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sec="http://secure.ws.documenta.com.py/">
					<soapenv:Header/>
					<soapenv:Body>
						<sec:consulta>
							<usuario>%s</usuario>
							<password>%s</password>

							<idServicio>%s</idServicio>
							<referenciaConsulta>%s</referenciaConsulta>
						</sec:consulta>
					</soapenv:Body>
				</soapenv:Envelope>
			`, username, password, serviceId, serviceReference,
		),
	))
}

func checkTransactionStatusRequest(username string, password string, trxID string) []byte {
	return []byte(
		strings.TrimSpace(
			fmt.Sprintf(
				`
					<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sec="http://secure.ws.documenta.com.py/">
						<soapenv:Header/>
						<soapenv:Body>
							<sec:estadoTransaccion>
									<usuario>%s</usuario>
									<password>%s</password>
									<trxRecaudador>%s</trxRecaudador>
							</sec:estadoTransaccion>
						</soapenv:Body>
					</soapenv:Envelope>
				`, username, password, trxID,
			),
		),
	)
}
